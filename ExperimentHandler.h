#pragma once

#include "Settings.h"
#include "State.h"
#include "FreeFunctions.h"

namespace dissertationCpp {

    struct ExperimentHandler {
        RNGType generator;
        boost::uniform_01<> getRandom01;


        explicit ExperimentHandler(unsigned int seed) : generator(seed) {
            //std::cout << sampleSumCache << std::endl;
        }

        State getState(const Settings &settings) {
            State state;
            boost::uniform_int<> getRandomLength(0, static_cast<int>(settings.indexToLength.size() - 1));
            for (auto i = 0; i < settings.size; i++) {
                //state.startConfigSpin.push_back(getRandomLength(generator));
                state.startConfigSpin.push_back(0);
            }

            //state.startConfigSpin;
            state.currentConfigSpin = state.startConfigSpin;
            state.minConfigSpin = state.startConfigSpin;
            state.currentT = settings.startT;
            state.currentE = getE(state.currentConfigSpin, settings);
            state.currentM = getM(state.currentConfigSpin, settings);

            //state.field;
            state.field.resize(settings.sample.size(), 0);
            auto fieldIndex = convertMToIndex(state.currentM, settings);
            state.field[fieldIndex] += 1;

            state.fieldSum = 1;
            state.minE = state.currentE;
            state.alpha = 1;
            //state.alphaArray;
            //state.tArray;
            //state.eArray;
            //state.mArray;

            return state;
        }

        template<bool T>
        void executeMKPEDD(State &state, const Settings &settings) {
            int countT = 0;
            int countStatistic = 0;

            if constexpr (T) {
                state.tArray.push_back(state.currentT);
                state.alphaArray.push_back(state.alpha);
            }

            /*
            for(auto i = 0; i < settings.countIteration; ++i) {
                for(auto j = 0; j < 0.1 * settings.countIteration; ++j) {
                    mk(state, settings);
                    if (state.currentE < state.minE) {
                        state.minE = state.currentE;
                        state.minConfigSpin = state.currentConfigSpin;
                    }

                    countT++;
                    if (countT >= 0.1 * settings.dropDeltaTPedd) {
                        state.currentT -= settings.percentTPedd * state.currentT;
                        countT = 0;
                    }

                    if constexpr (T) {
                        countStatistic++;
                        if (countStatistic == settings.deltaStatistic) {
                            state.tArray.push_back(state.currentT);
                            countStatistic = 0;
                        }
                    }
                    ++i;
                    if(i == settings.countIteration) break;
                }
                if(i == settings.countIteration) break;

                state.currentT = settings.startT;

                for(auto j = 0; j < settings.deltaPedd; ++j) {
                    pedd(state, settings);

                    if (state.currentE < state.minE) {
                        state.minE = state.currentE;
                        state.minConfigSpin = state.currentConfigSpin;
                    }

                    if constexpr (T) {
                        countStatistic++;
                        if (countStatistic == settings.deltaStatistic) {
                            state.tArray.push_back(state.currentT);
                            countStatistic = 0;
                        }
                    }
                    ++i;
                    if(i == settings.countIteration) break;
                }
            }
            */

            /*
            for (auto i = 0; i < settings.countIteration; ++i) {

                mk(state, settings);

                if (state.currentE < state.minE) {
                    state.minE = state.currentE;
                    state.minConfigSpin = state.currentConfigSpin;
                }

                //Понижаем температуру
                countT++;
                if (countT == settings.dropDeltaTPedd) {
                    state.currentT -= settings.percentTPedd * state.currentT;
                    countT = 0;


                    for (auto j = 0; j < settings.deltaPedd; ++j) {
                        pedd(state, settings);

                        if (state.currentE < state.minE) {
                            state.minE = state.currentE;
                            state.minConfigSpin = state.currentConfigSpin;
                        }

                        if constexpr (T) {
                            countStatistic++;
                            if (countStatistic == settings.deltaStatistic) {
                                state.tArray.push_back(state.currentT);
                                countStatistic = 0;
                            }
                        }
                        ++i;
                        if (i == settings.countIteration) break;
                    }

                }

                if constexpr (T) {
                    countStatistic++;
                    if (countStatistic == settings.deltaStatistic) {
                        state.tArray.push_back(state.currentT);
                        countStatistic = 0;
                    }
                }
            }
            */

            ///*
            int countAlpha = 0;
            for (auto i = 0; i < settings.countIteration; i++) {
                double beta = getRandom01(generator);
                if (state.alpha > beta) {
                    pedd(state, settings);
                    if(i > settings.deltaPedd)
                        countAlpha++;
                } else {
                    mk(state, settings);
                    countT++;
                }

                if (state.currentE < state.minE) {
                    state.minE = state.currentE;
                    state.minConfigSpin = state.currentConfigSpin;
                }

                if (countAlpha == settings.dropDeltaAlpha) {
                    state.alpha -= settings.percentAlpha * state.alpha;
                    countAlpha = 0;
                }

                if (countT == settings.dropDeltaTPedd) {
                    state.currentT -= settings.percentTPedd * state.currentT;
                    countT = 0;
                }

                if constexpr (T) {
                    countStatistic++;
                    if (countStatistic == settings.deltaStatistic) {
                        state.tArray.push_back(state.currentT);
                        state.alphaArray.push_back(state.alpha);
                        countStatistic = 0;
                    }
                }

            }
            //*/


        }

        template<bool T>
        void executeMK(State &state, const Settings &settings) {
            int countT = 0;
            int countStatistic = 0;

            if constexpr (T) {
                state.tArray.push_back(state.currentT);
            }

            for (auto i = 0; i < settings.countIteration; i++) {
                mk(state, settings);

                if (state.currentE < state.minE) {
                    state.minE = state.currentE;
                    state.minConfigSpin = state.currentConfigSpin;
                }

                //Понижаем температуру
                countT++;
                if (countT == settings.dropDeltaTMk) {
                    state.currentT -= settings.percentTMk * state.currentT;
                    countT = 0;
                }

                if constexpr (T) {
                    countStatistic++;
                    if (countStatistic == settings.deltaStatistic) {
                        state.tArray.push_back(state.currentT);
                        countStatistic = 0;
                    }
                }
            }
        }

        template<bool T>
        void executeOnlyPEDD(State &state, const Settings &settings) {
            int countStatistic = 0;

            if constexpr (T) {
                state.tArray.push_back(state.currentT);
            }

            for (auto i = 0; i < settings.countIteration; i++) {
                pedd(state, settings);

                if (state.currentE < state.minE) {
                    state.minE = state.currentE;
                    state.minConfigSpin = state.currentConfigSpin;
                }

                if constexpr (T) {
                    countStatistic++;
                    if (countStatistic == settings.deltaStatistic) {
                        state.tArray.push_back(state.currentT);
                        countStatistic = 0;
                    }
                }
            }
        }

        template<bool keepStatistics>
        void executeSeparatedMKPEDD(State &state, const Settings &settings) {
            if constexpr (T) {
                state.tArray.push_back(state.currentT);
                state.alphaArray.push_back(state.alpha);
            }

            for (auto i = 0; i < settings.deltaPedd; i++) {
                double beta = getRandom01(generator);
                if (state.alpha > beta) {
                    pedd(state, settings);

                } else {
                    mk(state, settings);
                }

                if (state.currentE < state.minE) {
                    state.minE = state.currentE;
                    state.minConfigSpin = state.currentConfigSpin;
                }

                countAlpha++;
                if (countAlpha == settings.dropDeltaAlpha) {
                    state.alpha -= settings.percentAlpha * state.alpha;
                    countAlpha = 0;
                }

                if constexpr (T) {
                    countStatistic++;
                    if (countStatistic == settings.deltaStatistic) {
                        state.tArray.push_back(state.currentT);
                        state.alphaArray.push_back(state.alpha);
                        countStatistic = 0;
                        //std::cout << "Statistic update!" << std::endl;
                    }
                }
            }

            for (auto i = settings.deltaPedd; i < settings.countIteration; i++) {
                mk(state, settings);

                if (state.currentE < state.minE) {
                    state.minE = state.currentE;
                    state.minConfigSpin = state.currentConfigSpin;
                }

                //Понижаем температуру
                countT++;
                if (countT == settings.dropDeltaT) {
                    state.currentT -= settings.percentTPedd * state.currentT;
                    countT = 0;
                }

                if constexpr (T) {
                    countStatistic++;
                    if (countStatistic == settings.deltaStatistic) {
                        state.tArray.push_back(state.currentT);
                        state.alphaArray.push_back(state.alpha);
                        countStatistic = 0;
                    }
                }
            }


        }


    private:

        void pedd(State &state, const Settings &settings) {
            auto leftRightShift = getLeftRightM(state, settings);
            std::shuffle(leftRightShift.begin(), leftRightShift.end(), generator);

            double currentH = std::numeric_limits<double>::infinity();
            auto shiftIndex = 0;

            for (auto i = 0; i < leftRightShift.size(); i++) {
                auto shift = leftRightShift[i];
                state.currentConfigSpin[shift.first] += shift.second;

                auto newM = state.currentM + shift.second;
                auto fieldIndex = convertMToIndex(newM, settings);

                auto h1 = settings.sample[fieldIndex] -
                          static_cast<double>(settings.sampleSum) / (state.fieldSum + 1) *
                          (state.field[fieldIndex] + 1);
                if (h1 < 0) h1 = 0;

                auto h2 = settings.sample[fieldIndex] -
                          static_cast<double>(settings.sampleSum) / state.fieldSum *
                          (state.field[fieldIndex]);
                if (h2 < 0) h2 = 0;

                auto h = h1 - h2;

                if (h < currentH) {
                    currentH = h;
                    shiftIndex = i;
                }
                state.currentConfigSpin[shift.first] -= shift.second;
            }
            auto shift = leftRightShift[shiftIndex];

            auto oldLocalE = getLocalE(state.currentConfigSpin, settings, shift.first);
            state.currentConfigSpin[shift.first] += shift.second;
            auto newLocalE = getLocalE(state.currentConfigSpin, settings, shift.first);
            auto delta = newLocalE - oldLocalE;

            state.currentM += shift.second;
            state.currentE += delta;

            auto fieldIndex = convertMToIndex(state.currentM, settings);
            state.field[fieldIndex] += 1;
            state.fieldSum += 1;
        }

        void mk(State &state, const Settings &settings) {
            boost::uniform_int<> getRandomSpin(0, settings.size - 1);
            auto index = getRandomSpin(generator);
            //auto oldEnergy = state.currentE;
            //auto oldM = state.currentM;

            int direction;
            if (state.currentConfigSpin[index] + 1 == settings.indexToLength.size())
                direction = -1;
            else if (state.currentConfigSpin[index] - 1 < 0)
                direction = 1;
            else {
                auto beta = getRandom01(generator);
                if (beta > 0.5)
                    direction = 1;
                else
                    direction = -1;
            }

            auto oldLocalE = getLocalE(state.currentConfigSpin, settings, index);
            state.currentConfigSpin[index] += direction;

            auto newLocalE = getLocalE(state.currentConfigSpin, settings, index);
            state.currentConfigSpin[index] -= direction;

            auto delta = newLocalE - oldLocalE;

            if (delta < 0) {
                state.currentConfigSpin[index] += direction;
                state.currentE += delta;
                state.currentM += direction;
            } else {
                auto w = std::exp(-delta / (settings.kBoltzmann * state.currentT));
                auto beta = getRandom01(generator);
                if(beta <= w) {
                    state.currentConfigSpin[index] += direction;
                    state.currentE += delta;
                    state.currentM += direction;
                }
            }

            auto fieldIndex = convertMToIndex(state.currentM, settings);
            state.field[fieldIndex] += 1;
            state.fieldSum += 1;
        }

        std::vector<std::pair<int, int>> getLeftRightM(const State &state, const Settings &settings) {
            std::vector<std::pair<int, int>> result;

            auto indexToLengthSize = static_cast<int>(settings.indexToLength.size());
            for (auto i = 0; i < state.currentConfigSpin.size(); ++i) {
                auto val = state.currentConfigSpin[i];
                if (val > 0) result.emplace_back(i, -1);
                if (val < indexToLengthSize - 1) result.emplace_back(i, 1);
            }

            /*
            bool leftGet = false;
            bool rightGet = false;
            auto indexToLengthSize = static_cast<int>(settings.indexToLength.size());

            for(auto i = 0; i < randomIndexSpins.size(); ++i) {
                auto val = state.currentConfigSpin[i];
                if (!leftGet && val > 0)  {
                    result.emplace_back(i, -1);
                    leftGet = true;
                }
                if (!rightGet && val < indexToLengthSize - 1) {
                    result.emplace_back(i, 1);
                    rightGet = true;
                }
                if(leftGet && rightGet) break;
            }
            */

            /*
            std::vector<int> notSmallArray;
            std::vector<int> notBigArray;
            auto indexToLengthSize = static_cast<int>(settings.indexToLength.size());
            for (auto i = 0; i < state.currentConfigSpin.size(); ++i) {
                auto val = state.currentConfigSpin[i];
                if (val > 0) notSmallArray.push_back(i);
                if (val < indexToLengthSize - 1) notBigArray.push_back(i);
            }

            bool leftGet = notSmallArray.empty();
            bool rightGet = notBigArray.empty();

            if (!leftGet) {
                boost::uniform_int<> getRandomValInRange(0, static_cast<int>(notSmallArray.size()) - 1);
                auto randIndex = notSmallArray[getRandomValInRange(generator)];
                //std::cout << randIndex << std::endl;
                result.emplace_back(randIndex, -1);
            }
            if (!rightGet) {
                boost::uniform_int<> getRandomValInRange(0, static_cast<int>(notBigArray.size()) - 1);
                auto randIndex = notBigArray[getRandomValInRange(generator)];
                //std::cout << randIndex << std::endl;
                result.emplace_back(randIndex, 1);
            }
            */


            //if (result.size() <= 1) {
            //    std::cout << "leftRightConfigs.size() == 0 " << std::endl;
            //    boost::mpi::environment::abort(1);
            //    }
            return result;
        }
    };
}