#pragma once
#include "Settings.h"

namespace dissertationCpp {
    struct State {
        friend class boost::serialization::access;

        //Base
        std::vector<int> startConfigSpin;
        std::vector<int> currentConfigSpin;
        std::vector<int> minConfigSpin;
        double currentE;
        int currentM;
        double minE;

        //MK
        double currentT;

        //PEDD
        std::vector<int> field;
        int fieldSum;

        //Base and Separate PEDD
        double alpha;

        //statistic info
        std::vector<double> alphaArray;
        std::vector<double> tArray;
        std::vector<double> eArray;
        std::vector<double> mArray;

        template<class Archive>
        void serialize(Archive &ar, const unsigned int version) {
            ar & startConfigSpin;
            ar & currentConfigSpin;
            ar & minConfigSpin;
            ar & currentE;
            ar & currentM;
            ar & minE;

            ar & currentT;

            ar & field;
            ar & fieldSum;

            ar & alpha;

            ar & alphaArray;
            ar & tArray;
            ar & eArray;
            ar & mArray;
        }
    };
}