#pragma once

#include "Settings.h"
#include "State.h"
#include "Libs/csv.h"

namespace dissertationCpp {
    BOOST_FORCEINLINE static double getE(const std::vector<int> &configSpin, const Settings &settings) {
        const auto &indexToLength = settings.indexToLength;
        const auto &interactionArray = settings.interactionArray;

        std::vector<double> sumE(interactionArray.size(), 0);
        double sumM = 0;
        for (auto i = 0; i < configSpin.size(); i++) {
            //double sum = 0;
            for (auto j = 0; j < sumE.size(); j++) {
                auto rightIndex = i + j + 1;
                //if (rightIndex >= configSpin.size()) rightIndex -= settings.size;
                if (rightIndex >= configSpin.size()) continue;
                sumE[j] += indexToLength[configSpin[i]] * indexToLength[configSpin[rightIndex]];
            }
            sumM += indexToLength[configSpin[i]];
        }

        double energy = 0;
        for (size_t i = 0; i < sumE.size(); i++) {
            energy -= interactionArray[i] * sumE[i];
        }
        energy -= settings.h * sumM;
        return energy;

        /*
        const auto &indexToLength = settings.indexToLength;
        const auto &interactionArray = settings.interactionArray;
        double sumFirst = 0;
        double sumSecond = 0;
        double sumThird = 0;
        for (auto i = 0; i < configSpin.size(); i++) {
            auto j = (i != configSpin.size() - 1) ? i + 1 : 0;
            auto k = (i < configSpin.size() - 2) ? i + 2 :
                     (i != configSpin.size() - 1) ? 0 : 1;

            auto s1 = indexToLength[configSpin[i]];
            auto s2 = indexToLength[configSpin[j]];
            auto s3 = indexToLength[configSpin[k]];
            sumFirst += s1;
            sumSecond += s1 * s2;
            sumThird += s1 * s3;
        }

        return -settings.h * sumFirst - interactionArray[0] * sumSecond - interactionArray[1] * sumThird;
        */
    }

    BOOST_FORCEINLINE static double
    getLocalE(const std::vector<int> &configSpin, const Settings &settings, int spinIndex) {
        const auto &indexToLength = settings.indexToLength;
        const auto &interactionArray = settings.interactionArray;

        std::vector<int> localConfigSpin;
        int minIndex = spinIndex - static_cast<int>(settings.interactionArray.size());
        if (minIndex < 0) minIndex = 0;
        int maxIndex = spinIndex + static_cast<int>(settings.interactionArray.size()) + 1;
        if (maxIndex >= configSpin.size()) maxIndex = static_cast<int>(configSpin.size());
        for (auto i = minIndex; i < maxIndex; ++i) {
            localConfigSpin.push_back(configSpin[i]);
        }
        return getE(localConfigSpin, settings);
    }


    BOOST_FORCEINLINE static int getM(const std::vector<int> &configSpin, const Settings &settings) {
        double m = 0;
        for (auto i: configSpin) {
            m += settings.indexToLength[i];
        }
        return static_cast<int>(m);
    }

    BOOST_FORCEINLINE static int convertIndexToM(int i, const Settings &settings) {
        auto maxLengthSpin = std::fabs(settings.indexToLength[0]);
        return static_cast<int>(i - maxLengthSpin * settings.size);
    }

    BOOST_FORCEINLINE static int convertMToIndex(int m, const Settings &settings) {
        auto maxLengthSpin = std::fabs(settings.indexToLength[0]);
        return static_cast<int>(m + maxLengthSpin * settings.size);
    }

    static mp::cpp_int factorial(int number) {
        mp::cpp_int num = 1;
        for (int i = 1; i <= number; i++)
            num = num * i;
        return num;
    }

    static mp::cpp_int Cnk(int x, const Settings &settings) {
        int spin0 = static_cast<int>(settings.size / 2 - x);
        int spin1 = settings.size - spin0;
        mp::cpp_int fac0 = factorial(spin0 + spin1);
        mp::cpp_int fac1 = factorial(spin0);
        mp::cpp_int fac2 = factorial(spin1);
        return static_cast<mp::cpp_int>(fac0 / (fac1 * fac2));
    }

    static std::vector<int> getSample(const Settings &settings) {

        auto maxLengthSpin = std::fabs(settings.indexToLength[0]);
        auto mSize = static_cast<int>(2 * maxLengthSpin * settings.size + 1);
        std::vector<int> sample(mSize, 8);
        //return sample;

        double mMax = maxLengthSpin * settings.size;
        auto gaussian = [mMax](double x) {
            return std::exp(-log(mMax * mMax) / (mMax * mMax) * x * x);
        };

        for (auto i = 0; i < mSize; i++) {
            if (i == 0 || i == mSize - 1) {
                sample[i] = 1;
                continue;
            }
            auto m = convertIndexToM(i, settings);
            auto alpha = gaussian(m);
            sample[i] = static_cast<int>(alpha * settings.countIteration);
            if (sample[i] < 10) sample[i] = 10;
        }

        /*
        auto maxLengthSpin = std::fabs(settings.indexToLength[0]);
        auto mSize = static_cast<int>(2 * maxLengthSpin * settings.size + 1);
        std::vector<mp::cpp_int> sample(mSize, mp::int1024_t(100));

        double a = 1000000;
        double b = 0;
        double c = 0.2 * settings.size;

        auto gaussian = [a, b, c](double x) {
            return a * std::exp(-(x - b) * (x - b) / (2 * c * c));
        };

        for (auto i = 0; i < mSize; i++) {
            auto m = convertIndexToM(i, settings);
            auto sampleVal = gaussian(m);
            sample[i] += mp::cpp_int(sampleVal);
        }
        */

        /*
        auto maxLengthSpin = std::fabs(settings.indexToLength[0]);
        auto mSize = static_cast<int>(2 * maxLengthSpin * settings.size + 1);
        std::vector<int> sample(mSize, 0);
        mp::cpp_rational alpha = mp::cpp_rational(settings.countIteration) / Cnk(0, settings);

        for (auto i = 0; i < mSize; i++) {
            if(i == 0 || i == mSize - 1) {
                sample[i] = 1;
                continue;
            }
            auto m = convertIndexToM(i, settings);
            sample[i] = (alpha * Cnk(m, settings)).convert_to<int>();
            if(sample[i] < 10) sample[i] = 10;
        }
        */

        return sample;
    }

    static Settings getSetting(const std::vector<double> &interactionArray, double h, double startT) {
        Settings settings;

        auto inputPath = std::string("settings.csv");
        io::CSVReader<12> baseInfo(inputPath);
        baseInfo.read_header(io::ignore_extra_column, "countIteration", "countExperiment", "size", "kBoltzmann",
                             "deltaPedd", "dropDeltaTMk", "percentTMk", "dropDeltaTPedd", "percentTPedd",
                             "dropDeltaAlpha", "percentAlpha", "deltaStatistic");
        int countIteration;
        int countExperiment;
        int size;
        int kBoltzmann;
        double deltaPedd;
        double dropDeltaTMk;
        double percentTMk;
        double dropDeltaTPedd;
        double percentTPedd;
        double dropDeltaAlpha;
        double percentAlpha;
        double deltaStatistic;
        while (baseInfo.read_row(countIteration, countExperiment, size, kBoltzmann, deltaPedd, dropDeltaTMk, percentTMk,
                                 dropDeltaTPedd, percentTPedd, dropDeltaAlpha, percentAlpha, deltaStatistic)) {
            settings.countIteration = countIteration;
            settings.countExperiment = countExperiment;
            settings.size = size;
            settings.kBoltzmann = kBoltzmann;
            settings.deltaPedd = static_cast<int>(deltaPedd * countIteration);
            settings.dropDeltaTMk = static_cast<int>(dropDeltaTMk * settings.countIteration);
            settings.percentTMk = percentTMk;
            settings.dropDeltaTPedd = static_cast<int>(dropDeltaTPedd * settings.countIteration);
            settings.percentTPedd = percentTPedd;
            settings.dropDeltaAlpha = static_cast<int>(dropDeltaAlpha * settings.countIteration);
            settings.percentAlpha = percentAlpha;
            settings.deltaStatistic = static_cast<int>(deltaStatistic * settings.countIteration);
            break;
        }

        settings.interactionArray = interactionArray;
        settings.h = h;

        settings.startT = startT;

        io::CSVReader<1> indexToLengthInfo(inputPath);
        indexToLengthInfo.read_header(io::ignore_extra_column, "indexToLength");
        double spinLength;
        while (indexToLengthInfo.read_row(spinLength)) {
            settings.indexToLength.push_back(spinLength);
        }

        settings.sample = getSample(settings);
        settings.sampleSum = std::accumulate(settings.sample.begin(), settings.sample.end(), 0);
        return settings;
    }

}
