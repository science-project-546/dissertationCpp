#include <iostream>
#include <boost/mpi.hpp>
#include "Settings.h"
#include "MainRankHandler.h"
#include "SupportRankHandler.h"
#include "Libs/csv.h"

namespace mpi = boost::mpi;

int main(int argc, char *argv[]) {
    using namespace dissertationCpp;
    mpi::environment env;
    mpi::communicator world;

    if(world.size() < 2) {
        std::cout << "Need more rank!" << std::endl;
        return 0;
    }

    std::vector<std::tuple<double,double,double,double,double,double>> models;

    if (world.rank() == 0) {
        double j1 = 0;
        double j2 = 0;
        double j3 = 0;
        double j4 = 0;
        //double j5 = 0;
        //double j6 = 0;
        double h = 0;
        double t = 0;

        constexpr int modelInfoSize = 6;
        std::string fileName = "input.csv";
        io::CSVReader<modelInfoSize> baseInfo(fileName);
        baseInfo.read_header(io::ignore_extra_column, "j1", "j2", "j3", "j4", "h", "t");

        while (baseInfo.read_row(j1, j2, j3, j4, h, t)) {
            models.emplace_back(j1,j2,j3,j4,h,t);
        }
    }

    int countModels = static_cast<int>(models.size());
    broadcast(world, countModels, 0);

    for(auto i = 0; i < countModels; ++i) {
        if (world.rank() == 0) {
            auto tuple = models[i];
            Settings settings{
                getSetting(
                        {get<0>(tuple), get<1>(tuple), /*get<2>(tuple), get<3>(tuple)*/},
                        get<4>(tuple), get<5>(tuple))
            };
            MainRankHandler rank;
            std::cout << "Start program " << i << "!" << std::endl;
            rank.execute(settings);
        }
        else {
            SupportRankHandler rank;
            rank.execute();
        }

        if(world.rank() == 0) {
            std::cout << "Ended program " << i << "!" << std::endl;
        }
    }

    return 0;
}