#include <boost/range/irange.hpp>
#include "settings.h"
#include "SampleGenerator.h"

namespace dissertationCpp {

    OldSettings OldSettings::getSettings() {
        auto countIteration = static_cast<int>(1e6);
        auto countExperiment = static_cast<int>(100);
        auto settings = OldSettings(countIteration, countExperiment, 1000,
                                    8, -4, 1, 40, 1, true,
                                    static_cast<int>(0.001 * countIteration),
                                    static_cast<int>(0.001 * countIteration),
                                    static_cast<int>(0.5 * countIteration));
        settings.withPPED = true;
        auto a = 2;
        for (auto i: boost::irange(-a, a + 2)) settings.indexToLength.push_back(i - 0.5);

        settings.sample = SampleGenerator(settings).Get();
        std::cout << "OldSettings created!" << std::endl;
        return settings;
    }
}