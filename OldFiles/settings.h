#pragma once
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/timer/timer.hpp>
#include <boost/mpi.hpp>

namespace dissertationCpp {
    typedef boost::mt19937 RNGType;
    namespace mp = boost::multiprecision;
    namespace mpi = boost::mpi;
    namespace timer = boost::timer;

    class OldSettings {
        friend class boost::serialization::access;

    public:
        int countIteration;
        int countExperiment;

        int size;
        double jOne;
        double jTwo;
        double h;
        double startT;
        double kBoltzmann;
        bool withPPED;

        // Счётчик определящий когда надо понизить температуру
        int maxCountBeforeTemperatureDrop;
        // Такой же но только для alpha
        int maxCountBeforeAlphaDrop;
        // Когда надо начать
        int maxStepAlphaStartDrop;

        std::vector<mp::int1024_t> sample;
        std::vector<double> indexToLength;

    private:
        template<class Archive>
        void serialize(Archive &ar, const unsigned int version) {
            ar & countIteration;
            ar & countExperiment;

            ar & size;
            ar & jOne;
            ar & jTwo;
            ar & h;
            ar & startT;
            ar & kBoltzmann;
            ar & withPPED;

            ar & maxCountBeforeTemperatureDrop;
            ar & maxCountBeforeAlphaDrop;
            ar & maxStepAlphaStartDrop;

            ar & sample;
            ar & indexToLength;
        }

        OldSettings(int countIteration, int countExperiment, int size, double jOne, double jTwo, double h, double startT,
                    double kBoltzmann, bool withPPED, int maxCountBeforeTemperatureDrop, int maxStepBeforeAlphaDrop,
                    int maxStepAlphaStartDrop)
                : countIteration(countIteration), countExperiment(countExperiment), size(size), jOne(jOne), jTwo(jTwo),
                  h(h), startT(startT), kBoltzmann(kBoltzmann), withPPED(withPPED),
                  maxCountBeforeTemperatureDrop(maxCountBeforeTemperatureDrop),
                  maxCountBeforeAlphaDrop(maxStepBeforeAlphaDrop),
                  maxStepAlphaStartDrop(maxStepAlphaStartDrop) {}


    public:
        OldSettings() = default;

        static OldSettings getSettings();
    };
}