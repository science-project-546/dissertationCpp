#pragma once
#include <boost/multiprecision/cpp_int.hpp>
#include <utility>
#include "settings.h"

namespace dissertationCpp {
    namespace mp = boost::multiprecision;

    class SampleGenerator {
    private:

        OldSettings settings;

    public:

        explicit SampleGenerator(OldSettings settings) : settings(std::move(settings)) {};

        std::vector<mp::int1024_t> Get();

    private:

        static mp::int1024_t factorial(int number);

        void composition(int n, int m, std::vector<int> vec, std::vector<std::vector<int>>& arr) const;

        /*Возращает все возможные комбинации длин спинов
        * Например если динна принимает зн. от -5/2 до 5/2 (6 возможных значений)
        * а конфигурация имеет длинну 10
        * То функция вернёт все возможные вектора типа (a, b, c, d, e, f)
        * где сумма a + ... + f = 10 при этом с учётом порядка
        * n - длинна конфигурации, m - все значения длин спина
        */
        std::vector<std::vector<int>> getAllComposition(int n, int m) const;

        mp::int1024_t OldCnk(int x) const;

        mp::int1024_t Cnk(std::vector<int> vec) const;

        int getMWithCombination(std::vector<int> combination) const;

        int convertMToIndex(int m) const;

        int convertIndexToM(int32_t i) const;
    };
}