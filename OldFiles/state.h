#pragma once
#include <cmath>
#include <vector>
#include <boost/range/counting_range.hpp>
#include <boost/range/irange.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include "boost/random.hpp"
#include "boost/generator_iterator.hpp"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include "settings.h"
#include <chrono>
#include <boost/mpi.hpp>
#include <boost/mpi/communicator.hpp>

namespace dissertationCpp {
    namespace mp = boost::multiprecision;
    typedef boost::mt19937 RNGType;

    class OldState {
        friend class boost::serialization::access;

    public:
        int size;
        double jOne;
        double jTwo;
        double h;
        double startT;
        double kBoltzmann;
        std::vector<int> startConfigSpin;
        std::vector<double> indexToLength;
        std::vector<mp::int1024_t> sample;
        mp::int1024_t sampleSum;

        double currentT;
        std::vector<int> configSpin;
        std::vector<mp::int1024_t> field;
        mp::int1024_t fieldSum;
        double minE;
        double alpha;
        int maxM;

        std::vector<double> alphaArray;
        std::vector<double> TArray;
        std::vector<double> EArray;
        std::vector<int> MArray;


    public:

        int convertIndexToM(int32_t i) const;

        int convertMToIndex(int m) const;

        int getM() const;

        int getMaxM();

        double getE() const;

        double getStartE() const;

    private:

        void createSpinConfig(boost::variate_generator<RNGType, boost::uniform_int<> >& dist);

        void createSampleAndField();

        template<class Archive>
        void serialize(Archive &ar, const unsigned int version) {
            ar & size;
            ar & jOne;
            ar & jTwo;
            ar & h;
            ar & startT;
            ar & kBoltzmann;
            ar & startConfigSpin;
            ar & indexToLength;
            ar & sample;
            ar & sampleSum;
            ar & currentT;
            ar & configSpin;
            ar & field;
            ar & fieldSum;
            ar & minE;
            ar & alpha;
            ar & alphaArray;
            ar & TArray;
            ar & EArray;
            ar & MArray;
        }

    public:

        explicit OldState(OldSettings setting, boost::variate_generator<RNGType, boost::uniform_int<> >& dist)
                : size(setting.size), jOne(setting.jOne), jTwo(setting.jTwo),
                  h(setting.h), startT(setting.startT), kBoltzmann(setting.kBoltzmann), indexToLength(setting.indexToLength), sample(setting.sample), currentT(setting.startT),
                  minE(FP_INFINITE) {
            maxM = -1;
            createSpinConfig(dist);
            createSampleAndField();
        }

        OldState() = default;
    };

}