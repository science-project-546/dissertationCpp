#pragma once
#include <boost/filesystem.hpp>
#include <vector>
#include "state.h"

namespace dissertationCpp {
    namespace fs = boost::filesystem;

    class InputOutputWorker {
    private:
        std::string resPath;

    public:

        InputOutputWorker() {
            resPath = std::string("Result/");
            if (!fs::is_directory(resPath)) boost::filesystem::create_directories(resPath);
        }

        void save(const std::vector<OldState> &states, const OldSettings& settings, long elapsed);

        OldSettings load();

    private:

        void saveMainInfo(const std::vector<OldState> &states, const OldSettings& settings, long time, const std::string& path);

        void saveStateInfo(const std::vector<OldState> &states, const OldSettings& settings, const std::string& path);

        std::string getTextConfig(OldState state, bool isBegin = false);
        
        void saveToCVS(const std::vector<OldState> &states, const std::string& path);

        void saveFirstSampleField(const std::vector<OldState> &states, const std::string& path);

        void saveE(const std::vector<OldState> &states, const std::string& path);

        void saveM(const std::vector<OldState> &states, const std::string& path);

        void saveAlpha(const std::vector<OldState> &states, const std::string& path);

        void saveT(const std::vector<OldState> &states, const std::string& path);

    };
}