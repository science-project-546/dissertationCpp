#include "state.h"


namespace dissertationCpp {

    int OldState::convertIndexToM(int i) const {
        auto maxLengthSpin = std::fabs(indexToLength[0]);
        return static_cast<int>(i - maxLengthSpin * size);
    }

    int OldState::convertMToIndex(int m) const {
        auto maxLengthSpin = std::fabs(indexToLength[0]);
        return static_cast<int>(m + maxLengthSpin * size);
    }

    int OldState::getM() const {
        double result = 0;
        for (auto i: configSpin) result += indexToLength[i];
        return static_cast<int>(result);
    }

    int OldState::getMaxM() {
        if (maxM == -1) {
            double result = 0;
            auto maxLengthSpin = std::fabs(indexToLength[0]);
            for (auto i = 0; i < size; i++) result += maxLengthSpin;
            maxM = static_cast<int>(result);
        }
        return maxM;
    }

    double OldState::getE() const {
        double sumFirst = 0;
        double sumSecond = 0;
        double sumThird = 0;
        for (auto i = 0; i < configSpin.size(); i++) {
            auto j = (i != configSpin.size() - 1) ? i + 1 : 0;
            auto k = (i < configSpin.size() - 2) ? i + 2 :
                     (i != configSpin.size() - 1) ? 0 : 1;

            auto s1 = indexToLength[configSpin[i]];
            auto s2 = indexToLength[configSpin[j]];
            auto s3 = indexToLength[configSpin[k]];
            sumFirst += s1;
            sumSecond += s1 * s2;
            sumThird += s1 * s3;
        }
        return -h * sumFirst - jOne * sumSecond - jTwo * sumThird;
    }

    double OldState::getStartE() const {
        double sumFirst = 0;
        double sumSecond = 0;
        double sumThird = 0;
        for (auto i = 0; i < startConfigSpin.size(); i++) {
            auto j = (i != startConfigSpin.size() - 1) ? i + 1 : 0;
            auto k = (i < startConfigSpin.size() - 2) ? i + 2 :
                     (i != startConfigSpin.size() - 1) ? 0 : 1;

            auto s1 = indexToLength[startConfigSpin[i]];
            auto s2 = indexToLength[startConfigSpin[j]];
            auto s3 = indexToLength[startConfigSpin[k]];
            sumFirst += s1;
            sumSecond += s1 * s2;
            sumThird += s1 * s3;
        }
        return -h * sumFirst - jOne * sumSecond - jTwo * sumThird;
    }

    void OldState::createSpinConfig(boost::variate_generator<RNGType, boost::uniform_int<> >& dist) {
        for (auto i = 0; i < size; i++) startConfigSpin.push_back(dist());
        configSpin = startConfigSpin;
    }

    void OldState::createSampleAndField() {
        //pped sample
        sampleSum = std::accumulate(sample.begin(), sample.end(), mp::int1024_t(0));
        field.resize(sample.size(), 0);
        auto fieldIndex = convertMToIndex(getM());
        field[fieldIndex] += 1;
        fieldSum += 1;
    }

};
