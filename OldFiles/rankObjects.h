#pragma once

#include <boost/mpi.hpp>
#include "settings.h"
#include "state.h"
#include "InputOutputWorker.h"
#include "ExperimentWorker.h"
#include <string>
#include <boost/date_time.hpp>
#include <boost/chrono.hpp>
#include <boost/timer/timer.hpp>

namespace dissertationCpp {
    namespace mpi = boost::mpi;

    //static std::string startMsg = "start";
    //static std::string endMsg = "end";

    struct MainRank {
        OldSettings settings;

        void execute() {
            mpi::environment env;
            mpi::communicator world;
            std::string startMsg = "start";
            std::string endMsg = "end";

            boost::timer::auto_cpu_timer timer;

            settings = OldSettings();
            broadcast(world, settings, 0);
            boost::uniform_int<> d(0, settings.indexToLength.size() - 1);
            boost::variate_generator<RNGType, boost::uniform_int<> >
                    dist(RNGType(time(nullptr)), d);

            std::vector<OldState> states;
            auto fakeState = OldState(settings, dist);

            for (int currentExperiment = 0; currentExperiment != settings.countExperiment;) {
                broadcast(world, startMsg, 0);
                for (int i = 1; i < world.size(); ++i) {
                    auto state = OldState(settings, dist);
                    world.send(i, 0, state);
                }

                std::vector<OldState> rankStates;
                gather(world, fakeState, rankStates, 0);

                for (auto i = 1; i < rankStates.size(); i++) {
                    std::cout << "Experiment " + std::to_string(currentExperiment) + " completed!" << std::endl;
                    states.push_back(rankStates[i]);
                    currentExperiment++;
                    if (currentExperiment == settings.countExperiment)
                        break;
                }
            }

            timer.stop();
            broadcast(world, endMsg, 0);
            boost::chrono::duration<double> elapsed = boost::chrono::nanoseconds(timer.elapsed().user);
            auto worker = InputOutputWorker();
            worker.save(states, settings, elapsed.count());

            /*
            broadcast(world, settings, 0);
            std::vector<OldState> states;
            int currentExperiment = 0;
            while (true) {

                std::vector<OldState> rankStates;
                auto fakeState = OldState(settings);
                broadcast(world, startMsg, 0);
                gather(world, fakeState, rankStates, 0);
                for (auto i = 1; i < rankStates.size(); i++) {
                    std::cout << "Experiment " + std::to_string(currentExperiment) + " completed!" << std::endl;
                    states.push_back(rankStates[i]);
                    currentExperiment++;
                    if (currentExperiment == settings.countExperiment)
                        break;
                }
                if (currentExperiment == settings.countExperiment)
                    break;
            }
            timer.stop();
            boost::chrono::duration<double> elapsed = boost::chrono::nanoseconds(timer.elapsed().user);
            broadcast(world, endMsg, 0);
            auto worker = InputOutputWorker();
            worker.save(states, settings, elapsed.count());
            */
        }
    };

    struct SupportRank {
        OldSettings settings;

        void execute() {
            mpi::environment env;
            mpi::communicator world;

            OldSettings settings;
            broadcast(world, settings, 0);

            while (true) {
                std::string msg;
                broadcast(world, msg, 0);
                if (msg == "end") break;
                if (msg == "start") {
                    OldState state;
                    world.recv(0, 0, state);
                    auto worker = ExperimentWorker(state, settings);
                    state = worker.execute();
                    gather(world, state, 0);
                }
            }
        }
    };

};