#include "ExperimentWorker.h"

namespace dissertationCpp {

    std::vector<std::pair<int, int>> ExperimentWorker::getLeftRightM() {
        auto m = state.getM();
        std::vector<std::pair<int, int>> result;

        std::vector<int> notSmallArray;
        std::vector<int> notBigArray;
        int indexToLengthSize = state.indexToLength.size();
        for (auto i = 0; i < state.configSpin.size(); ++i) {
            auto val = state.configSpin[i];
            if (val > 0) notSmallArray.push_back(i);
            if (val < indexToLengthSize - 1) notBigArray.push_back(i);
            if (val <= 0 || val >= indexToLengthSize - 1) {
                //std::cout << "val = " << static_cast<int>(val) << std::endl;
            }
        }

        auto getRandomValInRange = [this](int b) {
            double val = this->getRandom01();
            //std::cout << "Rand val = " << static_cast<int>(val * b) << std::endl;
            return static_cast<int>(std::floor(val * b));
        };

        bool leftGet = notSmallArray.empty();
        bool rightGet = notBigArray.empty();

        int direction = -1;
        if (!leftGet) {
            auto randIndex = notSmallArray[getRandomValInRange(notSmallArray.size() - 1)];
            state.configSpin[randIndex] += direction;
            auto newM = state.getM();
            state.configSpin[randIndex] -= direction;
            if (newM < m) {
                result.emplace_back(randIndex, direction);
                leftGet = true;
            }
        }

        direction = 1;
        if (!rightGet) {
            auto randIndex = notBigArray[getRandomValInRange(notBigArray.size() - 1)];
            state.configSpin[randIndex] += direction;
            auto newM = state.getM();
            state.configSpin[randIndex] -= direction;
            if (newM > m) {
                result.emplace_back(randIndex, direction);
                rightGet = true;
            }
        }

        if (result.size() == 0) {
            std::cout << "leftRightConfigs.size() == 0 " << std::endl;
            boost::mpi::environment::abort(1);
        }

        /*
        bool leftGet = m == -state.getMaxM();
        bool rightGet = m == state.getMaxM();
        std::vector<std::pair<int, int>> result;

        for (int i = 0;; ++i) {
            auto index = getRandomSpin();

            int direction = -1;
            if (!leftGet && state.configSpin[index] != 0) {
                state.configSpin[index] += direction;
                auto newM = state.getM();
                state.configSpin[index] -= direction;
                if (newM < m) {
                    result.emplace_back(index, direction);
                    leftGet = true;
                }
            }

            direction = 1;
            if (!rightGet && state.configSpin[index] != state.indexToLength.size() - 1) {
                state.configSpin[index] += direction;
                auto newM = state.getM();
                state.configSpin[index] -= direction;
                if (newM > m) {
                    result.emplace_back(index, direction);
                    rightGet = true;
                }
            }

            if (leftGet && rightGet) break;

            if (i > 10000) {
                std::cout << "max M = " << state.getMaxM() << std::endl;
                std::cout << "M = " << state.getM() << std::endl;
                std::cout << "error getLeftRightM!" << std::endl;
                std::cout << "error getLeftRightM!" << std::endl;
                std::cout << "leftGet " << leftGet << std::endl;
                std::cout << "rightGet " << rightGet << std::endl;
                boost::mpi::environment::abort(1);
            }
        */
        return result;
    }

    void ExperimentWorker::pedd() {
        auto leftRightConfigs = getLeftRightM();
        auto &configSpin = state.configSpin;

        mp::int1024_t currentH = std::numeric_limits<mp::int1024_t>::infinity();
        auto currentIndex = 0;
        auto currentDirection = 0;

        for (auto value: leftRightConfigs) {
            configSpin[value.first] += value.second;

            auto fieldIndex = state.convertMToIndex(state.getM());

            auto h1 = state.sample[fieldIndex] -
                      state.sampleSum / (state.fieldSum + 1) * (state.field[fieldIndex] + 1);
            if (h1 < 0)
                h1 = 0;

            auto h2 = state.sample[fieldIndex] - state.sampleSum / state.fieldSum * (state.field[fieldIndex]);
            if (h2 < 0)
                h2 = 0;

            auto h = h1 - h2;

            if (h < currentH) {
                currentH = h;
                currentIndex = value.first;
                currentDirection = value.second;
            }
            configSpin[value.first] -= value.second;
        }

        configSpin[currentIndex] += currentDirection;
        auto fieldIndex = state.convertMToIndex(state.getM());
        state.field[fieldIndex] += 1;
        state.fieldSum += 1;
    }

    void ExperimentWorker::mk() {
        auto index = getRandomSpin();
        auto &configSpin = state.configSpin;
        auto oldEnergy = state.getE();

        auto val = 0;
        if (state.configSpin[index] + 1 == state.indexToLength.size())
            val = -1;
        else if (state.configSpin[index] - 1 < 0)
            val = 1;
        else {
            auto beta = getRandom01();
            if (beta > 0.5)
                val = 1;
            else
                val = -1;
        }

        configSpin[index] += val;
        auto newEnergy = state.getE();
        auto delta = newEnergy - oldEnergy;
        if (delta > 0) {
            auto w = std::exp(-delta / (state.kBoltzmann * state.currentT));
            auto beta = getRandom01();
            if (beta > w)
                configSpin[index] -= val;
        }

        auto fieldIndex = state.convertMToIndex(state.getM());
        state.field[fieldIndex] += 1;
        state.fieldSum += 1;
    }

    OldState ExperimentWorker::execute() {
        if (settings.withPPED) {
            auto maxStepAlphaStartDrop = settings.maxStepAlphaStartDrop;
            auto countAlpha = 0;
            auto countT = 0;
            state.alpha = static_cast<double>(1);
            for (auto i = 0; i < settings.countIteration; i++) {
                auto beta = getRandom01();
                if (state.alpha > beta) {
                    pedd();
                } else {
                    mk();
                }

                if (i > maxStepAlphaStartDrop)
                    countAlpha++;
                countT++;

                auto currentE = state.getE();
                if (currentE < state.minE)
                    state.minE = currentE;


                if (countAlpha == settings.maxCountBeforeAlphaDrop) {
                    state.alpha -= 0.01 * state.alpha;
                    countAlpha = 0;

                }
                if (countT == settings.maxCountBeforeTemperatureDrop) {
                    state.currentT -= 0.01 * state.currentT;
                    countT = 0;
                }

                if (i % 1000 == 0) {
                    state.alphaArray.push_back(state.alpha);
                    state.TArray.push_back(state.currentT);
                    state.EArray.push_back(currentE);
                    state.MArray.push_back(state.getM());
                }
            }
        } else {
            auto count = 0;

            for (auto i = 0; i < settings.countIteration; i++) {
                mk();
                auto currentE = state.getE();
                if (currentE < state.minE)
                    state.minE = currentE;

                //Понижаем температуру
                count += 1;
                if (count == settings.maxCountBeforeTemperatureDrop) {
                    state.currentT -= 0.01 * state.currentT;
                    count = 0;
                }

                if (i % 1000 == 0) {
                    state.alphaArray.push_back(state.alpha);
                    state.TArray.push_back(state.currentT);
                    state.EArray.push_back(currentE);
                    state.MArray.push_back(state.getM());
                }
            }
        }
        return state;
    }
}