#include <iostream>
#include "csv.h"
#include "../Settings.h"
#include "../FreeFunctions.h"
#include "../ExperimentHandler.h"
#include "../OutputHandler.h"


int main(int argc, char *argv[]) {
    using namespace dissertationCpp;

    std::vector<double> interactionArray;
    interactionArray.push_back(8);
    interactionArray.push_back(4);
    double h = 0.01;
    Settings settings{getSetting(interactionArray, h)};

    boost::random_device rd("/dev/urandom");
    ExperimentHandler experimentHandler(rd());
    OutputHandler outputHandler;

    auto mkState = experimentHandler.getState(settings);
    auto peddState = experimentHandler.getState(settings);
    experimentHandler.executeMK<true>(mkState, settings);
    experimentHandler.executePEDD<true>(peddState, settings);
    //outputHandler.saveSupportInfo(mkState, peddState, settings);
    //outputHandler.savePart({mkState}, {peddState}, settings);
    //outputHandler.saveFinal(settings, 0);
    return 0;
}