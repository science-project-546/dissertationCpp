#pragma once
#include "state.h"
#include <boost/range/algorithm/random_shuffle.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/mpi.hpp>
#include <boost/mpi/communicator.hpp>

namespace dissertationCpp {
    class ExperimentWorker {
        boost::mpi::communicator world;

    private:
        OldState state;
        OldSettings settings;
        boost::variate_generator<RNGType, boost::uniform_int<> > getRandomSpin;
        boost::uniform_01<RNGType> getRandom01;

    private:
        std::vector<std::pair<int, int>> getLeftRightM();

        void pedd();

        void mk();

    public:
        explicit ExperimentWorker(OldState state, OldSettings settings)
                : state(state), settings(settings),
                  getRandomSpin(RNGType(time(0) + world.rank()), boost::uniform_int<>(0, settings.size - 1)),
                  getRandom01(RNGType(time(0) + world.rank())) {
        }

        OldState execute();
    };
}