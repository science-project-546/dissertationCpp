#include "SampleGenerator.h"
#include <boost/multiprecision/float128.hpp>
#include <boost/multiprecision/cpp_bin_float.hpp>
#include <boost/multiprecision/number.hpp>
#include <boost/mpi/environment.hpp>

namespace dissertationCpp {
    namespace mpns = boost::multiprecision;

    std::vector<mp::int1024_t> SampleGenerator::Get() {
        auto maxLengthSpin = std::fabs(settings.indexToLength[0]);
        //auto combinations = getAllComposition(settings.size, static_cast<int>(2 * maxLengthSpin + 1));

        auto mSize = static_cast<int>(2 * maxLengthSpin * settings.size + 1);
        std::vector<mp::int1024_t> sample(mSize, mp::int1024_t(100));

        double a = 1000000;
        double b = 0;
        double c = 0.2 * settings.size;

        auto gaussian = [a,b,c](int x) {
            return a * std::exp(-(x - b) * (x - b) / (2 * c * c));
        };

        for (auto i = 0; i < mSize; i++) {
            auto m = convertIndexToM(i);
            auto sampleVal = gaussian(m);
            sample[i] += mp::int1024_t(sampleVal);
        }

        //for (const auto &combination: combinations) {
        //    int m = getMWithCombination(combination);
        //     int i = convertMToIndex(m);
        //    sample[i] += Cnk(combination);
        //}
        return sample;
    }

    mp::int1024_t SampleGenerator::factorial(int number) {
        mp::int1024_t num = 1;
        for (int i = 1; i <= number; i++)
            num = num * i;
        return num;
    }

    void SampleGenerator::composition(int n, int m, std::vector<int> vec, std::vector<std::vector<int>>& arr) const {
        if (m == 0) {
            if (n == 0)
                arr.push_back(vec);
            return;
        }

        for (auto i = 0; i < n + 1; ++i) {
            std::vector<int> newVec = vec;
            newVec[m - 1] = i;
            composition(n - i, m - 1, newVec, arr);
        }
    }

    std::vector<std::vector<int>> SampleGenerator::getAllComposition(int n, int m) const {
        std::vector<std::vector<int>> arr;
        for (auto i = 0; i < n + 1; ++i) {
            std::vector<int> vec(m, 0);
            vec[m - 1] = i;
            composition(n - i, m - 1, vec, arr);
        }
        return arr;
    }

    mp::int1024_t SampleGenerator::OldCnk(int x) const {
        auto spin0 = static_cast<int>(settings.size / 2 - x);
        auto spin1 = settings.size - spin0;
        auto fac0 = factorial(spin0 + spin1);
        auto fac1 = factorial(spin0);
        auto fac2 = factorial(spin1);
        return static_cast<mp::int1024_t>(fac0 / (fac1 * fac2));
    }

    mp::int1024_t SampleGenerator::Cnk(std::vector<int> vec) const {
        mp::int1024_t result = factorial(std::accumulate(vec.begin(), vec.end(), 0));
        for (auto i = 0; i < vec.size(); i++) result /= factorial(vec[i]);
        return result;
    }

    int SampleGenerator::getMWithCombination(std::vector<int> combination) const {
        int m = 0;
        for (auto i = 0; i < combination.size(); i++) m += settings.indexToLength[i] * combination[i];
        return m;
    }

    int SampleGenerator::convertMToIndex(int m) const {
        auto maxLengthSpin = std::fabs(settings.indexToLength[0]);
        return static_cast<int>(m + maxLengthSpin * settings.size);
    }

    int SampleGenerator::convertIndexToM(int32_t i) const {
        auto maxLengthSpin = std::fabs(settings.indexToLength[0]);
        return static_cast<int>(i - maxLengthSpin * settings.size);
    }

    /*
    void OldState::createSampleAndField() {
        //pped sample
        sampleSum = 0;
        auto maxLengthSpin = std::fabs(indexToLength[0]);
        auto spinCount = static_cast<int>(2 * maxLengthSpin * size + 1);
        for (auto i = 0; i < spinCount; i++) {
            auto m = convertIndexToM(i);
            auto sampleVal = gaussian(m);
            sample.push_back(sampleVal);
            sampleSum += sampleVal;
        }

        field.resize(sample.size(), 0);
        auto fieldIndex = convertMToIndex(getM());
        field[fieldIndex] += 1;
        fieldSum += 1;
    }
     */


}
