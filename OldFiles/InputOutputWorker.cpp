#include "InputOutputWorker.h"
#include <boost/date_time.hpp>
#include <string>

namespace dissertationCpp {

    void InputOutputWorker::save(const std::vector<OldState> &states, const OldSettings &settings, long elapsed) {
        boost::posix_time::ptime timeLocal = boost::posix_time::second_clock::local_time();
        auto path = resPath + to_iso_extended_string(timeLocal);
        boost::filesystem::create_directories(path);
        saveMainInfo(states, settings, elapsed, path);
        saveStateInfo(states, settings, path);
        saveToCVS(states, path);
        saveFirstSampleField(states, path);
        saveE(states, path);
        saveM(states, path);
        saveT(states, path);
        saveAlpha(states, path);
    }

    OldSettings InputOutputWorker::load() {
        return OldSettings();
    }

    void InputOutputWorker::saveMainInfo(const std::vector<OldState> &states, const OldSettings &settings, long time,
                                         const std::string &path) {
        std::ofstream experimentFile(path + "/mainInfo.txt");

        auto minE = static_cast<double>(FP_INFINITE);
        auto meanMinE = static_cast<double>(0);
        for (const auto &state: states) {
            if (state.minE < minE)
                minE = state.minE;
            meanMinE += state.minE;
        }
        meanMinE /= static_cast<double>(states.size());

        auto countSuccess = 0;
        for (const auto &state: states)
            if (std::fabs(state.minE - minE) < std::numeric_limits<double>::epsilon())
                countSuccess += 1;

        double percentage = static_cast<double>(countSuccess) / states.size() * 100;

        auto text = std::string(
                "Time spent: " + std::to_string(time) + '\n'
                + "Max spin: " + std::to_string(std::fabs(states[0].indexToLength[0])) + '\n'
                + "Min E: " + std::to_string(minE) + '\n'
                + "Max step: " + std::to_string(settings.countIteration) + '\n'
                + "Equipartition min E: " + std::to_string(meanMinE) + '\n'
                + "Count experiments: " + std::to_string(states.size()) + '\n'
                + "Count success: " + std::to_string(percentage) + "%" + '\n'
        );
        experimentFile << text;
        experimentFile.close();
    }

    void InputOutputWorker::saveStateInfo(const std::vector<OldState> &states, const OldSettings &settings,
                                          const std::string &path) {
        std::ofstream experimentFile(path + "/statesInfo.txt");
        for (const auto &state: states) {
            auto beginConfig = getTextConfig(state, true);
            auto endConfig = getTextConfig(state);
            auto energy = state.minE;
            std::string text = "Begin Configuration: " + beginConfig + '\n'
                               + "End Configuration: " + endConfig + '\n'
                               + "Start T: " + std::to_string(state.startT) + '\n' \
 + "End T: " + std::to_string(state.currentT) + '\n' \
 + "Min E configuration: " + std::to_string(state.minE) + '\n';

            if (settings.withPPED)
                text += "Alpha: " + std::to_string(state.alpha) + '\n';

            text += '\n';

            experimentFile << text;
        }
        experimentFile.close();
    }

    std::string InputOutputWorker::getTextConfig(OldState state, bool isBegin) {
        std::string result = "(";

        auto &configSpin = isBegin ? state.startConfigSpin : state.configSpin;

        for (const auto &val: configSpin) {
            std::stringstream stream;
            stream << std::fixed << std::setprecision(2) << state.indexToLength[int(val)];
            result += stream.str() + ";";
        }
        result += ")";
        return result;
    }

    void InputOutputWorker::saveToCVS(const std::vector<OldState> &states, const std::string &path) {
        std::ofstream energyFile(path + "/energyInfo.csv");
        energyFile << "Start E" << "," << "Min E" << '\n';

        for (const auto &state: states) {
            energyFile << state.getStartE() << "," << state.minE << '\n';
        }
        energyFile.close();
    }

    void InputOutputWorker::saveFirstSampleField(const std::vector<OldState> &states, const std::string &path) {
        std::ofstream file(path + "/SampleField.csv");

        file << "M" << "," << "Sample" << ",";

        for(auto i = 0; i < states.size(); i++)
        {
            file << "Field_" << std::to_string(i) << ",";
        }

        file << '\n';

        auto baseState = states[0];
        for (auto i = 0; i < baseState.sample.size(); i++) {
            file << baseState.convertIndexToM(i) << ","
                 << baseState.sample[i] << ",";
            for(const auto & state : states)
            {
                file << state.field[i] << ",";
            }
            file << '\n';
        }

    }

    void InputOutputWorker::saveE(const std::vector<OldState> &states, const std::string &path) {
        std::ofstream file(path + "/EDynamicInfo.csv");

        file << "N" << ",";

        for(auto i = 0; i < states.size(); i++)
        {
            file << "Energy_" << std::to_string(i) << ",";
        }

        file << '\n';

        auto baseState = states[0];
        for (auto i = 0; i < baseState.EArray.size(); i++) {
            file << std::to_string(i) << ",";
            for(const auto & state : states)
            {
                file << state.EArray[i] << ",";
            }
            file << '\n';
        }
    }

    void InputOutputWorker::saveM(const std::vector<OldState> &states, const std::string &path) {
        std::ofstream file(path + "/MDynamicInfo.csv");

        file << "N" << ",";

        for(auto i = 0; i < states.size(); i++)
        {
            file << "M_" << std::to_string(i) << ",";
        }

        file << '\n';

        auto baseState = states[0];
        for (auto i = 0; i < baseState.MArray.size(); i++) {
            file << std::to_string(i) << ",";
            for(const auto & state : states)
            {
                file << state.MArray[i] << ",";
            }
            file << '\n';
        }
    }

    void InputOutputWorker::saveAlpha(const std::vector<OldState> &states, const std::string &path) {
        std::ofstream file(path + "/AlphaDynamicInfo.csv");

        file << "N" << ",";

        for(auto i = 0; i < states.size(); i++)
        {
            file << "Alpha_" << std::to_string(i) << ",";
        }

        file << '\n';

        auto baseState = states[0];
        for (auto i = 0; i < baseState.alphaArray.size(); i++) {
            file << std::to_string(i) << ",";
            for(const auto & state : states)
            {
                file << state.alphaArray[i] << ",";
            }
            file << '\n';
        }
    }

    void InputOutputWorker::saveT(const std::vector<OldState> &states, const std::string &path) {
        std::ofstream file(path + "/TDynamicInfo.csv");

        file << "N" << ",";

        for(auto i = 0; i < states.size(); i++)
        {
            file << "T_" << std::to_string(i) << ",";
        }

        file << '\n';

        auto baseState = states[0];
        for (auto i = 0; i < baseState.TArray.size(); i++) {
            file << std::to_string(i) << ",";
            for(const auto & state : states)
            {
                file << state.TArray[i] << ",";
            }
            file << '\n';
        }
    }

}