#pragma once

#include "Settings.h"
#include "State.h"
#include "ExperimentHandler.h"

namespace dissertationCpp {

    struct SupportRankHandler {
        SupportRankHandler() = default;

        void execute() {
            mpi::environment env;
            mpi::communicator world;

            Settings settings;
            unsigned int seed = 0;
            broadcast(world, settings, 0);
            world.recv(0, 0, seed);

            ExperimentHandler experimentHandler(seed);
            while (true) {
                enumCommand command;
                broadcast(world, command, 0);
                if (command == enumCommand::StartOnlyMK) {
                    auto state = experimentHandler.getState(settings);
                    experimentHandler.executeMK<false>(state, settings);
                    gather(world, state, 0);
                }
                if(command == enumCommand::StartWithPPED) {
                    auto state = experimentHandler.getState(settings);
                    experimentHandler.executeMKPEDD<false>(state, settings);
                    gather(world, state, 0);
                }
                if (command == enumCommand::End) {
                    break;
                }
            }
        }
    };
}