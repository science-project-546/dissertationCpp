#include "../OutputHandler.h"

int main(int argc, char *argv[]) {
    using namespace dissertationCpp;
    OutputHandler outputHandler;
    outputHandler.savePercentage();
    outputHandler.createPlotMeanEnergy();
    return 0;
}