#pragma once
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/timer/timer.hpp>
#include <boost/mpi.hpp>
#include <boost/filesystem.hpp>
#include <boost/date_time.hpp>
#include <boost/chrono/duration.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/range/irange.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/nondet_random.hpp>
#include <string>
#include <vector>

namespace dissertationCpp {
    typedef boost::mt19937 RNGType;
    namespace mp = boost::multiprecision;
    namespace mpi = boost::mpi;
    namespace timer = boost::timer;
    namespace fs = boost::filesystem;

    struct Settings {
        friend class boost::serialization::access;

        //Base
        int countIteration;
        int countExperiment;
        int size;
        std::vector<double> interactionArray;
        double h;
        std::vector<double> indexToLength;
        int deltaStatistic; // Когда надо занести число в статистику

        //MK
        double startT;
        double kBoltzmann;
        int dropDeltaTMk; // Значение, после которого надо понизить температуру
        double percentTMk; // На какой процент от текущего значения надо понизить температуру

        //PEDD
        int deltaPedd; // Сколько отводиться на алгоритм PEDD
        std::vector<int> sample;
        int sampleSum;
        int dropDeltaAlpha;
        double percentAlpha;

        //PEDD + MK
        int dropDeltaTPedd;
        double percentTPedd;

        template<class Archive>
        void serialize(Archive &ar, const unsigned int version) {
            ar & countIteration;
            ar & countExperiment;
            ar & size;
            ar & interactionArray;
            ar & h;
            ar & startT;
            ar & kBoltzmann;
            ar & deltaPedd;
            ar & indexToLength;
            ar & dropDeltaTMk;
            ar & percentTMk;
            ar & dropDeltaTPedd;
            ar & percentTPedd;
            ar & dropDeltaAlpha;
            ar & percentAlpha;
            ar & deltaStatistic;
            ar & sample;
            ar & sampleSum;
        }

        /*
        std::string getCSV() const {
            std::string result;
            result += "countIteration," + std::to_string(countIteration) + '\n';
            result += "countExperiment," + std::to_string(countExperiment) + '\n';
            result += "size," + std::to_string(size) + '\n';
            result += "Interaction";
            for (auto val : interactionArray)
                result += "," + std::to_string(val);
            result += '\n';
            result += "h," + std::to_string(h) + '\n';
            result += "startT," + std::to_string(startT) + '\n';
            result += "kBoltzmann," + std::to_string(kBoltzmann) + '\n';
            result += "deltaPedd," + std::to_string(deltaPedd) + '\n';
            result += "indexToLength";
            for (auto val : indexToLength)
                result += "," + std::to_string(val);
            result += '\n';
            result += "dropDeltaTMk," + std::to_string(dropDeltaTMk) + '\n';
            result += "percentTMk," + std::to_string(percentTMk) + '\n';
            result += "dropDeltaTPedd," + std::to_string(dropDeltaTPedd) + '\n';
            result += "percentTPedd," + std::to_string(percentTPedd) + '\n';
            result += "dropDeltaAlpha," + std::to_string(dropDeltaAlpha) + '\n';
            result += "percentAlpha," + std::to_string(percentAlpha) + '\n';
            result += "deltaStatistic," + std::to_string(deltaStatistic) + '\n';
            return result;
        }
        */
    };

    enum class enumCommand {
        StartOnlyMK,
        StartOnlyPEDD,
        StartBaseMKPEDD, //Изначальный вариант pedd
        StartSeparateMKPEDD, //Раздельный вариант
        StartCleaningMKPEDD, //Запуск PEDD для очистки между итеррациями MK
        End,
    };
}