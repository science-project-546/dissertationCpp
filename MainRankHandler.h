#pragma once

#include "Settings.h"
#include "State.h"
#include "ExperimentHandler.h"
#include "OutputHandler.h"

namespace dissertationCpp {

    struct MainRankHandler {

        void execute(Settings settings) {
            mpi::environment env;
            mpi::communicator world;

            timer::auto_cpu_timer timer;

            broadcast(world, settings, 0);

            boost::random_device rd("/dev/urandom");

            for (auto i = 1; i < world.size(); i++) {
                world.send(i, 0, rd());
            }

            ExperimentHandler experimentHandler(rd());
            OutputHandler outputHandler;

            /*
            auto checkForSimpleConfig = [](const std::vector<State> &states, const Settings &settings) {
                for (const auto &state: states) {
                    bool haveSimpleConfig = false;
                    auto minValCount = std::count(state.minConfigSpin.begin(), state.minConfigSpin.end(), 0);
                    if(minValCount == state.minConfigSpin.size()) return true;

                    auto maxValCount = std::count(state.minConfigSpin.begin(), state.minConfigSpin.end(), settings.indexToLength.size() - 1);
                    if(maxValCount == state.minConfigSpin.size()) return true;

                    for(auto i)
                }
                return false
            };
            */


            std::vector<State> mkStates;
            std::vector<State> peddStates;

            for (int currentExperiment = 0; currentExperiment < settings.countExperiment;) {

                experiment(mkStates, settings, enumCommand::StartOnlyMK, experimentHandler);
                experiment(peddStates, settings, enumCommand::StartWithPPED, experimentHandler);
                //std::cout << peddStates[0].currentT << std::endl;
                //std::cout << peddStates[0].alpha << std::endl;

                currentExperiment += world.size();
                std::cout << "Experiment " + std::to_string(currentExperiment) + " completed!" << std::endl;
                if (mkStates.size() > 1000) {
                    outputHandler.savePart(mkStates, peddStates, settings);
                    mkStates.clear();
                    peddStates.clear();
                }
            }

            if (!mkStates.empty()) {
                outputHandler.savePart(mkStates, peddStates, settings);
                mkStates.clear();
                peddStates.clear();
            }
            auto command = enumCommand::End;
            broadcast(world, command, 0);

            auto mkState = experimentHandler.getState(settings);
            auto peddState = experimentHandler.getState(settings);
            experimentHandler.executeMK<true>(mkState, settings);
            experimentHandler.executeMKPEDD<true>(peddState, settings);

            timer.stop();
            boost::chrono::duration<double> elapsed = boost::chrono::nanoseconds(timer.elapsed().user);
            outputHandler.saveFinal(mkState, peddState, settings, elapsed.count());
        }

        static void experiment(std::vector<State> &states, const Settings &settings, enumCommand command,
                               ExperimentHandler &experimentHandler) {
            mpi::environment env;
            mpi::communicator world;

            std::vector<State> tmpStates;
            broadcast(world, command, 0);
            auto state = experimentHandler.getState(settings);
            if (command == enumCommand::StartOnlyMK) {
                experimentHandler.executeMK<false>(state, settings);
            }
            if (command == enumCommand::StartWithPPED) {
                experimentHandler.executeMKPEDD<false>(state, settings);
            }
            gather(world, state, tmpStates, 0);
            for (auto &element: tmpStates) {
                states.push_back(element);
            }
        }
    };


}
