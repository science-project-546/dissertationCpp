#pragma once

#include "Settings.h"
#include "State.h"
#include "FreeFunctions.h"
#include "csv.h"
#include <stdlib.h>

namespace dissertationCpp {

    class OutputHandler {
    private:
        std::string resultFolder;

        std::string experimentFolder;
        std::string mainFile;
        std::string startEndEnergyFile;
        std::string configSpinFile;
        std::string tFile;
        std::string alphaFile;
        std::string fieldFile;

        std::string supportFolder;
        std::string plotPercentage;
        std::string plotMeanE;

    public:

        explicit OutputHandler() {
            resultFolder = std::string("Result/");
            if (!fs::is_directory(resultFolder)) boost::filesystem::create_directories(resultFolder);

            boost::posix_time::ptime timeLocal = boost::posix_time::second_clock::local_time();
            experimentFolder = resultFolder + to_iso_extended_string(timeLocal);
            mainFile = experimentFolder + "/main.csv";
            startEndEnergyFile = experimentFolder + "/startEndEnergy.csv";
            configSpinFile = experimentFolder + "/configSpin.csv";
            tFile = experimentFolder + "/t.csv";
            alphaFile = experimentFolder + "/alpha.csv";
            fieldFile = experimentFolder + "/field.csv";

            supportFolder = std::string("SupportResult/");
            if (!fs::is_directory(supportFolder)) boost::filesystem::create_directories(supportFolder);

            plotPercentage = supportFolder + "/plotPercentage.csv";
            plotMeanE = supportFolder + "/plotMeanE.csv";
        }

        //SAVE FINAL
        void saveFinal(const State &mkState, const State &peddState, const Settings &settings, double time) {
            saveMainFile(mkState, peddState, settings, time);
            saveT(mkState, peddState, settings);
            saveAlpha(peddState, settings);

            transposeFieldFile();
        }

        void saveMainFile(const State &mkState, const State &peddState,const Settings &settings, double time) {
            if (!fs::is_directory(experimentFolder)) {
                fs::create_directories(experimentFolder);
            }

            std::ofstream mainF(mainFile);

            mainF << "Time," + std::to_string(time) + '\n';
            mainF << settings.getCSV();

            mainF << "End T MK," << mkState.tArray.back() << '\n';
            mainF << "End T PEDD," << peddState.tArray.back() << '\n';
            mainF << "End alpha PEDD," << peddState.alphaArray.back() << '\n';

            io::CSVReader<2> in(startEndEnergyFile);
            in.read_header(io::ignore_extra_column, "MKMinE", "PEDDMinE");
            double mkMinE;
            double peddMinE;
            std::vector<double> mkMinEArray;
            std::vector<double> peddMinEArray;
            while (in.read_row(mkMinE, peddMinE)) {
                mkMinEArray.push_back(mkMinE);
                peddMinEArray.push_back(peddMinE);
            }

            auto outputPercentage = [&mainF](std::vector<double> &minEArray) {
                std::sort(minEArray.begin(), minEArray.end());
                auto minE = minEArray[0];
                auto count = std::count(minEArray.begin(), minEArray.end(), minE);
                auto percentage = static_cast<double>(count) /
                                  static_cast<double>(minEArray.size());
                mainF << "Min E," + std::to_string(minE) + '\n';
                mainF << "Count," + std::to_string(count) + '\n';
                mainF << "Percentage," + std::to_string(percentage * 100) + ", %\n";
            };

            mainF << "MK\n";
            outputPercentage(mkMinEArray);
            mainF << "pedd\n";
            outputPercentage(peddMinEArray);
            mainF.close();
        }

        void saveT(const State &mkState, const State &peddState, const Settings &settings) {
            std::ofstream f(tFile);
            f << "x,t_mk,t_pedd" << "\n";
            int j = 0;
            for (auto i = 0; i < peddState.tArray.size(); i++) {
                f << std::to_string(j) << ",";
                f << mkState.tArray[i] << ",";
                f << peddState.tArray[i] << "\n";
                j += settings.deltaStatistic;
            }
        }

        void saveAlpha(const State &peddState, const Settings &settings) {
            std::ofstream f(alphaFile);
            f << "x,alpha" << "\n";

            int j = 0;
            for (auto i = 0; i < peddState.tArray.size(); i++) {
                f << std::to_string(j) << ",";
                f << peddState.alphaArray[i] << "\n";
                j += settings.deltaStatistic;
            }
        }

        void transposeFieldFile() {
            auto tmp = experimentFolder + "/tmpField.csv";
            system(("csvtool transpose " + fieldFile + " > " + tmp).c_str());
            boost::filesystem::remove(fieldFile);
            boost::filesystem::rename(tmp, fieldFile);
        }

        //SAVE PART
        void savePart(const std::vector<State> &mkStates, const std::vector<State> &peddStates,
                      const Settings &settings) {

            if (!fs::is_directory(experimentFolder)) {
                fs::create_directories(experimentFolder);
            }

            assert(mkStates.size() == peddStates.size());
            saveStartMinEnergy(mkStates, peddStates, settings);
            saveMinConfig(mkStates, peddStates, settings);
            //saveT(mkStates, peddStates, settings);
            //saveAlpha(peddStates, settings);
            saveField(peddStates, settings);
        }

        void saveStartMinEnergy(const std::vector<State> &mkStates, const std::vector<State> &peddStates,
                                const Settings &settings) {
            if (!boost::filesystem::exists(startEndEnergyFile)) {
                std::ofstream f(startEndEnergyFile);
                f << "MKStartE,MKMinE,MagMK,PEDDStartE,PEDDMinE,MagPedd\n";
                f.close();
            }

            std::ofstream f(startEndEnergyFile, std::ofstream::app);
            for (auto i = 0; i < mkStates.size(); ++i) {

                f << getE(mkStates[i].startConfigSpin, settings) << "," << mkStates[i].minE << ",";
                f << getM(mkStates[i].minConfigSpin, settings) << ",";
                f << getE(peddStates[i].startConfigSpin, settings) << "," << peddStates[i].minE << ",";
                f << getM(peddStates[i].minConfigSpin, settings) << '\n';
            }
            f.close();
        }

        void saveMinConfig(const std::vector<State> &mkStates, const std::vector<State> &peddStates,
                           const Settings &settings) {
            if (!boost::filesystem::exists(configSpinFile)) {
                std::ofstream f(configSpinFile);
                f.close();
            }

            std::ofstream f(configSpinFile, std::ofstream::app);
            for (auto i = 0; i < mkStates.size(); ++i) {
                f << "MK," << getSpinTextConfig(mkStates[i].minConfigSpin, settings) << '\n';
                f << "PEDD," << getSpinTextConfig(peddStates[i].minConfigSpin, settings) << '\n';
            }
            f.close();
        }

        static std::string getSpinTextConfig(const std::vector<int> &configSpin, const Settings &settings) {
            std::string result = "(";

            for (const auto &val: configSpin) {
                std::stringstream stream;
                stream << std::fixed << std::setprecision(1) << settings.indexToLength[int(val)];
                result += stream.str() + ";";
            }
            result += ")";
            return result;

        }

        void saveField(const std::vector<State> &peddStates, const Settings &settings) {
            if (!boost::filesystem::exists(fieldFile)) {
                std::ofstream f(fieldFile);
                f << "m,";
                for (auto i = 0; i < peddStates[0].field.size(); i++) {
                    f << convertIndexToM(i, settings) << ", ";
                }
                f << "\n";

                f << "sample,";
                for (int i : settings.sample) {
                    f << i << ", ";
                }
                f << "\n";

                f.close();
            }

            std::ofstream f(fieldFile, std::ofstream::app);

            for(const auto & peddState : peddStates) {
                f << "field,";
                for(const auto& element : peddState.field) {
                    f << element  << ", ";
                }
                f << "\n";
            }
            f.close();
        }

        //WorkWithResult

        void savePercentage() {
            std::vector<std::tuple<double, double, double, double>> result;

            if (fs::is_directory(resultFolder)) {
                for (auto &entry: boost::make_iterator_range(fs::directory_iterator(resultFolder), {})) {
                    auto path = entry.path().string() + "/startEndEnergy.csv";
                    result.push_back(getPercentage(path));
                }
            }

            std::ofstream file(plotPercentage);
            file << "i,MK,PEDD,E_MK,E_PEDD,delta,PEDD_better\n";
            for (auto i = 0; i < result.size(); ++i) {
                file << i << "," << get<0>(result[i]) << "," << get<1>(result[i]) << ",";
                file << get<2>(result[i]) << "," << get<3>(result[i]) << ",";
                auto delta = get<3>(result[i]) - get<2>(result[i]);
                file << delta << ",";

                auto status = 0;

                if(fabs(delta) > FLT_EPSILON) {
                    if(delta < 0)
                        status = 1;
                    if(delta > 0)
                        status = -1;
                }

                file << status << "\n";
            }
        }

        void createPlotMeanEnergy() {
            std::vector<std::tuple<double, double, double>> result;

            if (fs::is_directory(resultFolder)) {
                for (auto &entry: boost::make_iterator_range(fs::directory_iterator(resultFolder), {})) {
                    auto path = entry.path().string() + "/startEndEnergy.csv";
                    result.push_back(getMeanE(path));
                }
            }

            std::ofstream file(plotMeanE);
            file << "i,MK,PEDD,delta\n";
            for (auto i = 0; i < result.size(); ++i) {
                file << i << ",";
                file << get<0>(result[i]) << ",";
                file << get<1>(result[i]) << ",";
                file << get<2>(result[i]) << "\n";
            }
        }

        static std::tuple<double, double, double, double> getPercentage(const std::string &path) {
            io::CSVReader<2> in(path);
            in.read_header(io::ignore_extra_column, "MKMinE", "PEDDMinE");
            double mkMinE;
            double peddMinE;
            std::vector<double> mkMinEArray;
            std::vector<double> peddMinEArray;
            while (in.read_row(mkMinE, peddMinE)) {
                mkMinEArray.push_back(mkMinE);
                peddMinEArray.push_back(peddMinE);
            }
            std::sort(mkMinEArray.begin(), mkMinEArray.end());
            std::sort(peddMinEArray.begin(), peddMinEArray.end());

            auto outputPercentage = [](std::vector<double> &minEArray) {
                auto minE = minEArray[0];
                auto count = std::count(minEArray.begin(), minEArray.end(), minE);
                auto percentage = static_cast<double>(count) /
                                  static_cast<double>(minEArray.size());
                return percentage;
            };

            return std::make_tuple(outputPercentage(mkMinEArray),
                                   outputPercentage(peddMinEArray),
                                   mkMinEArray[0],
                                   peddMinEArray[0]);
        }

        static std::tuple<double, double, double> getMeanE(const std::string &path) {
            io::CSVReader<2> in(path);
            in.read_header(io::ignore_extra_column, "MKMinE", "PEDDMinE");
            double mkMinE;
            double peddMinE;
            std::vector<double> mkMinEArray;
            std::vector<double> peddMinEArray;
            while (in.read_row(mkMinE, peddMinE)) {
                mkMinEArray.push_back(mkMinE);
                peddMinEArray.push_back(peddMinE);
            }
            auto mkMeanE = std::accumulate(mkMinEArray.begin(), mkMinEArray.end(), static_cast<double>(0));
            mkMeanE /= static_cast<int>(mkMinEArray.size());
            auto peddMeanE = std::accumulate(peddMinEArray.begin(), peddMinEArray.end(), static_cast<double>(0));
            peddMeanE /= static_cast<int>(peddMinEArray.size());
            return std::make_tuple(mkMeanE, peddMeanE, peddMeanE - mkMeanE);
        }

        //old
        /*


         void saveSupportInfo(const State &mkState, const State &peddState,
                             const Settings &settings) {
            saveSample(settings);
            saveAlpha(peddState, settings);
            saveT(mkState, peddState, settings);
        }

        void saveMainInfo
                (const std::vector<State> &states, const Settings &settings, double time, const std::string &path) {
            std::ofstream experimentFile(path + "/mainInfo.txt");

            auto minE = static_cast<double>(FP_INFINITE);
            auto meanMinE = static_cast<double>(0);
            for (const auto &state: states) {
                if (state.minE < minE)
                    minE = state.minE;
                meanMinE += state.minE;
            }
            meanMinE /= static_cast<double>(states.size());

            auto countSuccess = 0;
            for (const auto &state: states)
                if (std::fabs(state.minE - minE) < std::numeric_limits<double>::epsilon())
                    countSuccess += 1;

            double percentage = static_cast<double>(countSuccess) / static_cast<double>(states.size()) * 100;

            auto text = std::string(
                    "Time spent: " + std::to_string(time) + '\n'
                    + "Max spin: " + std::to_string(std::fabs(settings.indexToLength[0])) + '\n'
                    + "Size: " + std::to_string(settings.size) + '\n'
                    + "Min E: " + std::to_string(minE) + '\n'
                    + "Max step: " + std::to_string(settings.countIteration) + '\n'
                    + "Equipartition min E: " + std::to_string(meanMinE) + '\n'
                    + "Count experiments: " + std::to_string(states.size()) + '\n'
                    + "Count success: " + std::to_string(percentage) + "%" + '\n'
            );
            experimentFile << text;
            experimentFile.close();
        }

        static void saveStateInfo(const std::vector<State> &states, const Settings &settings, const std::string &path) {
            std::ofstream experimentFile(path + "/statesInfo.txt");
            for (const auto &state: states) {
                auto beginConfig = getTextConfig(state, settings, true);
                auto endConfig = getTextConfig(state, settings);
                auto energy = state.minE;
                std::string text = "Begin Configuration: " + beginConfig + '\n'
                                   + "End Configuration: " + endConfig + '\n'
                                   + "Start T: " + std::to_string(settings.startT) + '\n' \
 + "End T: " + std::to_string(state.currentT) + '\n' \
 + "Min E configuration: " + std::to_string(state.minE) + '\n';

                //if (settings.withPEDD)
                text += "Alpha: " + std::to_string(state.alpha) + '\n';

                text += '\n';

                experimentFile << text;
            }
            experimentFile.close();
        }

        static std::string getTextConfig(const State &state, const Settings &settings, bool isBegin = false) {
            std::string result = "(";

            auto &configSpin = isBegin ? state.startConfigSpin : state.configSpin;

            for (const auto &val: configSpin) {
                std::stringstream stream;
                stream << std::fixed << std::setprecision(2) << settings.indexToLength[int(val)];
                result += stream.str() + ";";
            }
            result += ")";
            return result;

        }

        static void saveToCVS(const std::vector<State> &states, const Settings &settings, const std::string &path) {
            std::ofstream energyFile(path + "/energyInfo.csv");
            energyFile << "Start E" << "," << "Min E" << '\n';

            for (const auto &state: states) {
                energyFile << getE(state.startConfigSpin, settings) << "," << state.minE << '\n';
            }
            energyFile.close();
        }

        static void
        saveFirstSampleField(const std::vector<State> &states, const Settings &settings, const std::string &path) {
            std::ofstream file(path + "/SampleField.csv");

            file << "M" << "," << "Sample" << ",";

            for (auto i = 0; i < states.size(); i++) {
                file << "Field_" << std::to_string(i) << ",";
            }

            file << '\n';

            for (auto i = 0; i < settings.sample.size(); i++) {
                file << convertIndexToM(i, settings) << ","
                     << settings.sample[i] << ",";
                for (const auto &state: states) {
                    file << state.field[i] << ",";
                }
                file << '\n';
            }
        }

        static void saveE(const std::vector<State> &states, const std::string &path) {
            std::ofstream file(path + "/EDynamicInfo.csv");

            file << "N" << ",";

            for (auto i = 0; i < states.size(); i++) {
                file << "Energy_" << std::to_string(i) << ",";
            }

            file << '\n';

            auto baseState = states[0];
            for (auto i = 0; i < baseState.eArray.size(); i++) {
                file << std::to_string(i) << ",";
                for (const auto &state: states) {
                    file << state.eArray[i] << ",";
                }
                file << '\n';
            }
        }

        static void saveM(const std::vector<State> &states, const std::string &path) {
            std::ofstream file(path + "/MDynamicInfo.csv");

            file << "N" << ",";

            for (auto i = 0; i < states.size(); i++) {
                file << "M_" << std::to_string(i) << ",";
            }

            file << '\n';

            auto baseState = states[0];
            for (auto i = 0; i < baseState.mArray.size(); i++) {
                file << std::to_string(i) << ",";
                for (const auto &state: states) {
                    file << state.mArray[i] << ",";
                }
                file << '\n';
            }
        }

        static void saveAlpha(const std::vector<State> &states, const std::string &path) {
            std::ofstream file(path + "/AlphaDynamicInfo.csv");

            file << "N" << ",";

            for (auto i = 0; i < states.size(); i++) {
                file << "Alpha_" << std::to_string(i) << ",";
            }

            file << '\n';

            auto baseState = states[0];
            for (auto i = 0; i < baseState.alphaArray.size(); i++) {
                file << std::to_string(i) << ",";
                for (const auto &state: states) {
                    file << state.alphaArray[i] << ",";
                }
                file << '\n';
            }
        }

        static void saveT(const std::vector<State> &states, const std::string &path) {
            std::ofstream file(path + "/TDynamicInfo.csv");

            file << "N" << ",";

            for (auto i = 0; i < states.size(); i++) {
                file << "T_" << std::to_string(i) << ",";
            }

            file << '\n';

            auto baseState = states[0];
            for (auto i = 0; i < baseState.tArray.size(); i++) {
                file << std::to_string(i) << ",";
                for (const auto &state: states) {
                    file << state.tArray[i] << ",";
                }
                file << '\n';
            }
        }
        */

        /*
        void saveT(const std::vector<State> &mkStates, const std::vector<State> &peddStates, const Settings &settings) {
            if (!boost::filesystem::exists(tModelFile)) {
                std::ofstream f(tModelFile);
                f << "x";
                for (auto i = 0; i < peddStates[0].tArray.size(); i++) {
                    f << "," << std::to_string(i);
                }
                f << "\n";
                f.close();
            }

            std::ofstream f(tModelFile, std::ofstream::app);

            for(auto i = 0; i < peddStates.size(); ++i) {
                f << "MK";
                for(const auto& element : mkStates[i].tArray) {
                    f << "," << element;
                }
                f << "\n";

                f << "PEDD";
                for(const auto& element : peddStates[i].tArray) {
                    f << "," << element;
                }
                f << "\n";
            }
            f.close();
        }

        void saveAlpha(const std::vector<State> &peddStates, const Settings &settings) {
            if (!boost::filesystem::exists(alphaModelFile)) {
                std::ofstream f(alphaModelFile);
                f << "x";
                for (auto i = 0; i < peddStates[0].alphaArray.size(); i++) {
                    f << "," << std::to_string(i);
                }
                f << "\n";
                f.close();
            }

            std::ofstream f(alphaModelFile, std::ofstream::app);

            for(auto i = 0; i < peddStates.size(); ++i) {
                f << "PEDD";
                for(const auto& element : peddStates[i].alphaArray) {
                    f << "," << element;
                }
                f << "\n";
            }
            f.close();
        }
        */

    };
}
