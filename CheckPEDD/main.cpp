#include <iostream>
#include "csv.h"
#include "../Settings.h"
#include "../FreeFunctions.h"
#include "../ExperimentHandler.h"
#include "../OutputHandler.h"


int main(int argc, char *argv[]) {
    using namespace dissertationCpp;

    std::vector<double> interactionArray = {-0.971604, 0.796374, 0.233827, 0.427459};
    double h = 0;
    double startT = 4;
    Settings settings{getSetting(interactionArray, h, startT)};
    boost::random_device rd("/dev/urandom");
    ExperimentHandler experimentHandler(rd());
    OutputHandler outputHandler;

    auto state1 = experimentHandler.getState(settings);
    experimentHandler.executeOnlyPEDD<true>(state1, settings);
    outputHandler.savePart({state1}, {state1}, settings);
    outputHandler.saveFinal({state1}, {state1}, settings, 0);
    return 0;
}