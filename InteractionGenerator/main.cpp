#include <boost/random/uniform_real.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/timer/timer.hpp>
#include <boost/filesystem.hpp>
#include <boost/random/random_device.hpp>
#include <vector>

int main(int argc, char *argv[]) {
    typedef boost::mt19937 RNGType;
    namespace fs = boost::filesystem;
    using namespace boost::random;

    std::string fileName = "input.csv";

    auto countInteraction = 4;

    if (!fs::exists(fileName)) {
        std::ofstream f(fileName);
        for(auto i = 1; i <= countInteraction; ++i) {
            f << "j" << i << ",";
        }
        f << "h,";
        f << "t\n";
        f.close();
    }

    std::ofstream f(fileName, std::ofstream::app);

    boost::random_device rd("/dev/urandom");
    RNGType generator(rd());

    auto maxRandJ = 1;

    boost::uniform_real<> getRandJ(0.1, maxRandJ);
    boost::uniform_01<> getRand01;

    for(auto i = 0; i < 10; ++i){
        std::vector<double> interactionArray;
        for(auto j = 0; j < countInteraction; ++j) {
            interactionArray.push_back(getRandJ(generator));
        }

        interactionArray[1] *= -1;
        for(auto j = 2; j < interactionArray.size(); j++) {
            if(getRand01(generator) > 0.5) {
                interactionArray[j] *= -1;
            }
        }
        std::shuffle(interactionArray.begin(), interactionArray.end(), generator);

        double h = 0;
        for(auto element : interactionArray)
        {
            if(std::fabs(element) > h)
                h = std::fabs(element);

        }

        boost::uniform_real<> getRandH(0, h);
        h = getRandH(generator);

        for(auto j : interactionArray) {
            f << j << ",";
        }
        f << h << ",";
        f << 4 * maxRandJ << "\n";
    }
    f.close();

    return 0;
}
